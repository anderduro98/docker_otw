# Utiliza una imagen base de Linux
FROM ubuntu:latest

# Actualiza los paquetes e instala Bash
RUN apt-get update && \
    apt-get install -y bash sshpass lftp openssh-server git

# Establece el directorio de trabajo
WORKDIR /app

RUN git clone https://gitlab.com/anderduro98/otw_auto_script

# Copia tus scripts de Bash al contenedor
#COPY script.sh /app/

# Establece permisos de ejecución para el script
RUN chmod +x otw_auto_script/script.sh

# Ejecuta el script cuando el contenedor se inicie
CMD ["otw_auto_script/script.sh"]
